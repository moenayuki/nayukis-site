% Copyright 2014, Nyk Ma. Under MIT License.
% 算法说明：假如原始向量是[0.6, 0.4, 0.2, 1.0, 0.8]
% 显然，升序排序后会变为[0.2, 0.4, 0.6, 0.8, 1.0]
% 原始向量的第一个元素0.6被映射到排序后的第三个位置
% 那么，变换向量的第一个元素就是3
% 同理可推其余元素。最后变换向量为[3, 2, 1, 5, 4]
% 使用这个变换向量来对目标大矩阵进行映射

clc;clear; % 环境初始化

x = rand(256,1);
y = rand(256,1); % 初始化原始向量，用你的数据源替换它
target = rand(256,256); % 目标矩阵
targetNew = zeros(256,256);

x_transform = zeros(256,1);
y_transform = zeros(256,1); % 变换向量

x_sorted = x;
y_sorted = y;

for i=1:256
    for j=1:256
        if (x(i) == x_sorted(j)) % 在排序后的向量中找原始向量里的元素
            x_transform(i) = j; % 将新元素的位置保存在变换向量
            break
        end
    end
end

for i=1:256
    for j=1:256
        if (y(i) == y_sorted(j))
            y_transform(i) = j;
            break
        end
    end
end

for i=1:256
    for j=1:256
        targetNew(i,x_transform(j)) = target(i,j); % 横向重排
    end
end

target = targetNew;
targetNew = zeros(256,256); % 横向重排结束。准备纵向重排

for i=1:256
    for j=1:256
        targetNew(y_transform(j),i) = target(j,i); % 纵向重怕·排
    end
end

disp(targetNew) % 此为最终结果