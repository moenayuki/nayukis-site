var utility = require('utility');

function md5cal (phrase, key1, key2) {
  var result = utility.md5(phrase + key1).slice(0,8);
  return utility.md5(result + key2);
}

function b64cal (phrase) {
  var result = new Buffer(phrase.slice(0,11)).toString('base64');
  result = result.split("").reverse().join("");
  return result.replace(/=/g, "!").replace(/l/g, "&");
}

exports.npg = function (req, res) {
  res.render('npg', {pageid: 'npg', headtitle: "NPG v0.5", originalkey: "", md5res: "", b64res: ""});
};

exports.plain = function (req, res) {
  var phrase = req.body.target;
  var key1 = req.body.key1;
  var key2 = req.body.key2;
  var result = ({md5: md5cal(phrase, key1, key2).slice(0,8), b64: b64cal(md5cal(phrase))});
  res.send(result);
};

exports.key = function (req, res) {
  res.render("key", {pageid: 'npgkey', headtitle: "NPG: Set keys"});
};