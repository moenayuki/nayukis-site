var validator = require('validator');
var cubieIP = "";

exports.post = function (req, res, next) {
  cubieIP = req.body.ip;
  var secret = req.body.secret;
  console.log("GET: " + req.body.ip);
  if(validator.isIP(req.body.ip)) {
    cubieIP = req.body.ip;
    res.send("SUCCESS");
  }
  else
    next();
};

exports.get = function (req, res) {
  console.log("/cubie GET!");
  res.send(cubieIP);
};
