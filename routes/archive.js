var fs = require('fs');
var path = require('path');
var _s = require('underscore.string');
var moment = require('moment');
var marked = require('marked');
var validator = require('validator');
//var extend = require('extend');
var raneto = require('raneto-core');
raneto.config = require('../config_blog.js');

exports.blog = function(req, res, next) {
  if(req.query.search){
    var searchQuery = validator.toString(validator.escape(_s.stripTags(req.query.search))).trim(),
      searchResults = raneto.doSearch(searchQuery),
      pageListSearch = raneto.getPages('');

    res.render('archive_search', {
      pageid: "archive",
      headtitle: '"' + searchQuery +'"的搜索结果 | Nayuki\'s Archive',
      config: config,
      pages: pageListSearch,
      search: searchQuery,
      searchResults: searchResults,
      body_class: 'page-search'
    });
  }
  else if(1) {
    var slug = req.params[0];
    if(!slug) slug = '/index';
    if(slug == '/' || slug == '/archive') slug = '/index';

    var pageList = raneto.getPages(slug),
      filePath = path.normalize(raneto.config.content_dir + slug);
    if(!fs.existsSync(filePath)) filePath += '.md';

    if(slug == '/index' && !fs.existsSync(filePath)){
      res.render('archive', {
        pageid: "archive",
        headtitle: "Nayuki's Archive",
        config: config,
        meta: {title: "Nayuki"},
        pages: pageList,
        body_class: 'page-home'
      });
    } else {
      fs.readFile(filePath, 'utf8', function(err, content) {
        if(err){
          err.status = '404';
          err.message = 'Whoops. Looks like this page doesn\'t exist.';
          res.redirect('/archive');
          next(err);
        }

        // Process Markdown files
        if(path.extname(filePath) == '.md' && fs.existsSync(filePath)) {
          // File info

          var stat = fs.lstatSync(filePath);
          // Meta
          var meta = raneto.processMeta(content);
          content = raneto.stripMeta(content);
          if(!meta.title) meta.title = raneto.slugToTitle(filePath);
          // Content
          content = raneto.processVars(content);
          var html = marked(content);
          var fpsent = filePath.split(path.sep);
          fpsent.shift(); // remove '/public/'
          fpsent = path.normalize(fpsent.join('/'));
          res.render('archive', {
            pageid: "archive",
            headtitle: meta.title +" | Nayuki's Archive",
            config: raneto.config,
            pages: pageList,
            meta: meta,
            content: html,
            filepath: fpsent,
            body_class: 'page-'+ raneto.cleanString(slug),
            written: moment(meta.date).format("YYYY-MM-DD"),
            last_modified: moment(stat.mtime).format('YYYY-MM-DD')
          });
        } else {
          next();
        }
      });
    }
  } else {
    console.log(req.params[0]);
    console.log("blogNexted");
    next();
  }
};

//// Handle any errors
//function blog_error(err, req, res) {
//  res.status(err.status || 500);
//  res.render('error', {
//    config: config,
//    status: err.status,
//    message: err.message,
//    error: {},
//    body_class: 'page-error'
//  });
//}
