var express = require('express');
var routes = require('./routes');
var npg = require('./routes/npg');
var resume = require('./routes/resume');
var archive = require('./routes/archive');
var ab = require('./routes/ab');
var cubie = require('./routes/cubie');
var morgan = require('morgan');
var jade = require('jade');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');

var app = express();
app.use(morgan('dev')); // log等级
app.set('views', './views'); // 设定模板位置
app.set('view engine', 'jade');
app.engine('jade', require('jade').__express); // 设定渲染引擎
app.use(express.static(path.join(__dirname, 'public'))); // 静态内容
app.use(favicon(path.join(__dirname, 'public', 'img', 'favicon.ico'))); // favicon
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.get('/', routes.index);

app.all('/archive', archive.blog);
app.all('/archive*', function (req, res, next) {
  archive.blog(req, res, next);
});

app.get('/npg', npg.npg);
app.get('/cv', resume.index);
app.get('/ab', ab.index);
app.get('/npg/key', npg.key);
app.post('/npg/plain', npg.plain);
app.post('/cubie', cubie.post);
app.get('/cubie', cubie.get);

app.get('/*', function (req, res) {
  res.redirect('/');
});

app.listen(9080);
console.log("TARGET....");
