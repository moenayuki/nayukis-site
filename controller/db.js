var dbController = {
  init: function (db, dbExists) {
    if(!dbExists) {
      db.run("CREATE TABLE Water(amount REAL, method TEXT, username TEXT, reason TEXT, time TEXT, comment TEXT);", function (err) {
        if(err)
          throw new Error("TABLE Water already exists.");
      });
      db.run("CREATE TABLE Wallet(method TEXT, balance REAL, creditline REAL, dueday INTEGER, comment TEXT);", function (err) {
        if(err)
          throw new Error("TABLE Wallet already exists.");
      });
      db.run("CREATE TABLE Users(username TEXT, nickname TEXT, pw TEXT, usergroup TEXT);", function (err) {
        if(err)
          throw new Error("TABLE Users already exists.");
      });
      //db.run("INSERT INTO Users VALUES('root','Root','sss','root')");
      return(1);
    }
  },

  query: function (db, username, offset, length, callback) {
    var queryString = "SELECT rowid AS id, * FROM Water WHERE username='" + username + "' ORDER BY ID DESC LIMIT " + length + " OFFSET " + offset + ";";
    db.serialize(function () {
      db.all(queryString, callback);
    });
  },

  ins: function (db, table, data) {
    db.serialize(function() {
      switch(table){
        case "Water":
          db.run("INSERT INTO Water VALUES(?,?,?,?,?,?);", data);
          break;
        case "Wallet":
          db.run("INSERT INTO Wallet VALUES(?,?,?,?,?);", data);
          break;
        case "Users":
          db.run("INSERT INTO Users VALUES(?,?,?,?);", data);
          break;
      }
  })},

  del: function (db, table, id) {
    db.serialize(function () {
      db.run("DELETE FROM ? WHERE rowid=?;", table, id);
    });
  }

  //refreshWallet: function () {
  //
  //}

};

module.exports = dbController;