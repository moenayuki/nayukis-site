var should = require('should');
var sqlite = require("sqlite3").verbose();
var db = new sqlite.Database('test/test.db');
var dbController = require('../controller/db.js');
var dbExists = require('fs').existsSync('test/test.db');

describe('../controller/db.js', function () {
  it('should init correctly.', function () {
    dbController.init(db).should.be.ok;
  });
  it('should add and read a record properly.', function () {
    dbController.ins(db, 'Water', [10.0, 'cash', 'test', 'a test', 'no']);
    dbController.query(db, 'test', 0, 10, function (qres) {
      qres.should.be.an.instanceOf(Object).and.have.property('username', 'test');
    });
  });

});